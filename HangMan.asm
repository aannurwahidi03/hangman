.include "m8515def.inc"

;====================================================================
; DECLARE
;====================================================================
.def highScore  	= r5	; 
.def score 			= r6	; 
.def ones			= r7	; Ones of a number
.def tens			= r8	; Tens of a number
.def isRight		= r9	; Indicates true or false input
.def totalTime		= r10	; Total time to guess questions
.def questionOrder	= r11	; 
.def letterWord 	= r12	; Letter from question (word) that is loaded from SRAM
.def letterAnswer 	= r13	; Letter from saved answer that is loaded from SRAM
.def input			= r14	; Input from keypad
.def type			= r15	; Type of hangman question
.def temp			= r16	; Temporary register
.def keyVal			= r17	; value input keypad
.def temp2			= r18	; Temporary Register
.def keyFlag		= r20	; Flag for keypad
.def toLCD			= r21	; Things that will be displayed on the LCD
.def countdown		= r22 
.def lives 			= r23	; Player's life
.def longWord		= r24	; Length of the word/question
.def letterGuessed	= r25	; How many words are correctly guessed
.def tester			= r26

; MEMORY ADDRESS and CONSTS
.equ pressed	= 0				; Bit for Pressed Flag (Flag if the key on the keypad is pressed)
.equ EN			= 0				; Bit for EN (LCD)
.equ RS			= 1				; Bit for RS (LCD)
.equ countdownTarget  = 3
.equ scorePerQuestion = 10	; Scoring for each correct answer
.equ words		= $60			; Address for Questions
.equ answers	= $80			; Address for Saved Answer (Letter that is correctly guessed)
.equ highScores	= $100			; Address for High Score

; INPUT and OUTPUT STUFFS
.equ LcdCtrlPort	= PORTA	; PORT A untuk LCD Data
.equ LcdCtrlDDR 	= DDRA
.equ LcdDataPort   	= PORTB	; PORT B untuk LCD Control (EN, RS)
.equ LcdDataDDR 	= DDRB
.equ KeyPadPort		= PORTC	; PORT C untuk Keypad
.equ KeyPadDDR		= DDRC
.equ LedPort    	= PORTE	; PORT E untuk LED
.equ LedDDR     	= DDRE

.equ cols = PINC		; PIN for columns on Keypad
.equ col1 = PINC0		; Column 1 (A, H, L, R)
.equ col2 = PINC1		; Column 2 (B, I, M, S)
.equ col3 = PINC2		; Column 3 (D, J, N, T)
.equ col4 = PINC3		; Column 4 (E, K, O, U)

;====================================================================
; MACROS
;====================================================================

.MACRO LOAD_QUESTION1					; Macro for Loading each question
		ldi letterGuessed, 0			; Set guessed letter to 0
		ldi lives, 0b00011111			; Set players life to 5 LEDs
		out LedPort, lives
		PRINT_QUESTION_TITLE @0			; Call Macro that writing the question title
		rcall QUESTION_TYPE				; Print Hangman Type to LCD
		rcall UNDERSCORE_TEXT			; Print Underscore to LCD
		rcall TURN_ON_TIMER				; Turn On Timer
		rcall GET_ANSWER				; Get Input from player
.ENDMACRO

.MACRO PRINT_FINAL_MESSAGE				; Macro for writing Final Message to LCD
		rcall TURN_OFF_TIMER			; Turn off timer
		rcall CLEAR_LCD					; Clear LCD
		MOVE_CURSOR 0x8E				; Move Cursor
		ldi ZH,high(2*@0) 				; Load high part of byte address into ZH
		ldi ZL,low(2*@0) 				; Load low part of byte address into ZL
		rcall LOADBYTE					; Load Message to LCD

		rcall DELAY_02 					; Delay
		rcall DELAY_02
		rcall DELAY_02
		rcall DELAY_02					; Delay
		rcall DELAY_02
		rcall DELAY_02
		rcall DELAY_02					; Delay
		rcall DELAY_02
		rcall DELAY_02

		rcall CLEAR_LCD	
	
		rjmp MAIN_PROGRAM 				; Jump to Main_program again
	.ENDMACRO

.MACRO PRINT_NUMBER 					; Macro for writing numbers to LCD
		MOVE_CURSOR @0 					; Move Cursor
		mov tens, @1 					; Move numbers to tens
		rcall BIN2DEC 					; Call Binary to Dec Function
		ldi temp, 48 					; Add Ones and Tens with 48 (To get the ASCIIs)
		add ones, temp
		add tens, temp
		WRITE_TEXT_WITHOUT_DELAY tens 	; Write Tens to LCD
		WRITE_TEXT_WITHOUT_DELAY ones 	; Write Ones to LCD
.ENDMACRO

.MACRO PRINT_QUESTION_TYPE 				; Macro for writing hangman type to LCD
		MOVE_CURSOR @0 	
		ldi ZH,high(2*@1) 				; Load high part of byte address into ZH
		ldi ZL,low(2*@1) 				; Load low part of byte address into ZL
		rcall LOADBYTE 					; Load Message to LCD				; Move Cursor

		
		;ldi ZH, high(@2) 				; Load high part of byte address into ZH (Type)
		;ldi ZL, low(@2) 				; Load low part of byte address into ZL
		ldi YH, high(words) 			; Load high part of byte address into YH (Question)
		ldi YL, low(words) 				; Load low part of byte address into YL
		ldi XH, high(answers) 			; Load high part of byte address into XH (Saved Answer)
		ldi XL, low(answers)			; Load low part of byte address into XL


		ldi longWord, 5					; Set Question length to 25 (All 5 questions with each question has 5 letters long)
		rcall FLASH2RAM					; Call function that write data from program memory to data memory
.ENDMACRO

.MACRO PRINT_QUESTION_TITLE				; Macro for writing question title to LCD
		rcall CLEAR_LCD					; Clear LCD
		MOVE_CURSOR 0x8D				; Move Cursor

		ldi ZH,high(2*@0) 				; Load high part of byte address into ZH
		ldi ZL,low(2*@0) 				; Load low part of byte address into ZL
		rcall LOADBYTE					; Load Message to LCD
		rcall DELAY_02					; Delay
		rcall DELAY_02
		rcall DELAY_02
.ENDMACRO

.MACRO MOVE_CURSOR						; Macro to move cursor to some address
		cbi LCDCtrlPort,RS				; CLR RS
		ldi temp,@0						; Move cursor
		out LCDDataPort,temp
		sbi LcdCtrlPort,EN				; SETB EN
		cbi LcdCtrlPort,EN				; CLR EN
.ENDMACRO

.MACRO WRITE_TEXT_WITHOUT_DELAY			; Macro for writing text to LCD without delay
		sbi LcdCtrlPort, RS 			; SETB RS
		out LcdDataPort, @0				; Write text
		sbi LcdCtrlPort, EN 			; SETB EN
		cbi LcdCtrlPort, EN				; CLR EN
.ENDMACRO

;====================================================================
; SUBROUTINE
;====================================================================

.org $00			; Hardware Reset Interrupt	
rjmp TOP
.org $01			; External Interrupt 0
rjmp START
.org $02			; External Interrupt 1
rjmp RESET
.org $07			; Internal Timer Overflow 0 Interrupt
rjmp TIMER

;====================================================================
; CODE SEGMENT
;====================================================================
;====================================================================
; INTERRUPT
;====================================================================

START:									; External Interrupt 0
	rcall TURN_OFF_TIMER				; Turn off timer
	ldi temp, 0				
	mov questionOrder, temp 			; Set question order to 0
	mov score, temp 					; Set score to 0
	rjmp TOP 							; Jump to Top

RESET:									; External Interrupt 1
	rcall TURN_OFF_TIMER				; Turn off timer
	ldi temp, 0
	mov questionOrder, temp 				; Set question order to 0
	mov highScore, temp 				; Set High Score to 0
	mov score, temp 					; Set Score to 0
	rjmp TOP							; Jump to Top

TIMER: 									; Internal Timer Overflow 0 Interrupt
	inc countdown 						; If countdown  == 3, check the time
	ldi temp, countdownTarget
	cp countdown, temp
	breq CHECK_TIME
	reti
	
	CHECK_TIME:
		clr countdown 					; Clear countdown
		mov temp, totalTime 			; Decrement total time
		dec temp
		mov totalTime, temp
		rcall DECREASE_TIME 			; Write remaining time to LCD
		tst totalTime 					; If time == 0, write time is up message
		breq TIME_IS_UP
		reti

		TIME_IS_UP:
			rcall DECREASE_TIME 		; Write remaining time to LCD
			clr temp 					; Turn off timer
			out TIFR, temp
			out TIMSK, temp
			sei
			PRINT_FINAL_MESSAGE timeisup ; Call Macro that write the final message with timeisup message as parameter


DECREASE_TIME:
	PRINT_NUMBER 0xA5, totalTime		; Print Remaining time
	ret

TURN_ON_TIMER:							; init to activate the timer
	ldi temp, 1<<TOV0					; set the TOV0 to 1 (timer overflow)
	out TIFR, temp						; Interrupt if overflow occurs in T/C0
	ldi temp, 1<<TOIE0					; set TOIE0 to 1 (to enable timer overflow interrupt)
	out TIMSK, temp						; Enable Timer/Counter0 Overflow int
	ldi temp, 31						; set temporary to 31 seconds (actual time = 30 seconds)
	mov totalTime, temp					; move temp to specific register
	sei									; enable the interrupt
	ret									; return to previous address

TURN_OFF_TIMER: 						; Turn off time
	clr temp
	out TIFR, temp 						; Set TIFR to 0
	out TIMSK, temp 					; Set TIMSK to 0
	sei
	ret

;====================================================================
; INIT
;====================================================================
TOP:

INIT_STACK:					; Set stack pointer
	ldi temp, low(RAMEND)
	out SPL, temp
	ldi temp, high(RAMEND)
	out SPH, temp

INIT_KEYPAD:				; Set Keypad
	ldi temp, $F0     		; Rows in keypad as output, columns as input
	out KeyPadDDR, temp
	ldi temp, $0F
	out KeyPadPort, temp

INIT_LED:					; init to set up the LED
	ser lives				; load $FF to lives
	out LedDDR, lives		; mov the number of the lives to LedDDR
	ldi lives, 0b00011111	; set 5 LEDs on
	out LedPort, lives		; light up the LED

INIT:
	rcall INIT_LCD			; Init LCD
	ser temp
	out LcdCtrlDDR, temp 	; Set port A as output
	out LcdDataDDR, temp 	; Set port B as output

INIT_INTERRUPT:				; init to set the interrupt
	ldi temp,0b11000000		; enable bit 7 & 6 to set INT0 & INT1 on
	out GICR,temp			; set the GICR
	ldi temp,0b00001010		; set int0 & int1 generate interrupts on falling edge
	out MCUCR,temp			; set the MCUCR
	sei						; enable the interrupt

INIT_TIMER:					; init to set the timer
	ldi temp, (1<<CS02)		; set bit SC02 to 1 (timer1 Pck/256)
	out TCCR0, temp			; set the TCCR

;====================================================================
; MAIN PROGRAM
;====================================================================
MAIN_PROGRAM:
	rcall WELCOME_AND_CATEGORY_TEXT		; Write Beginning text to LCD
	rcall INPUT_WORD					; Get input for Hangman Type

	LOAD_QUESTION1 question1 			; Load question 1
	
	rjmp WIN 							; Jump to win condition
		

BACK:
	ret

INPUT_WORD:
	ldi YH, high(words)
	ldi YL, low(words)

	rcall GET_INPUT						; Get input from keypad
	mov input, toLCD
	mov tester, input				    
	st Y+, tester
	rcall WRITE_TEXT

	rcall GET_INPUT						; Get input from keypad
	mov input, toLCD
	mov tester, input
	st Y+, tester
	rcall WRITE_TEXT

	rcall GET_INPUT						; Get input from keypad
	mov input, toLCD
	mov tester, input
	st Y+, tester
	rcall WRITE_TEXT

	rcall GET_INPUT						; Get input from keypad
	mov input, toLCD
	mov tester, input
	st Y+, tester
	rcall WRITE_TEXT

	rcall GET_INPUT						; Get input from keypad
	mov input, toLCD
	mov tester, input
	st Y+, tester
	rcall WRITE_TEXT


	rcall DELAY_02					; Delay
	rcall DELAY_02
	rcall DELAY_02


	ret
	


GET_ANSWER:
	in r0, LedPort						; If player's life == 0, game over
	tst r0
	breq LOSE 							; Branch to lose condition

	cpi letterGuessed, 5 				; If total of correctly guessed letter == 5, change the word/question
	breq RESTART

	rcall GET_INPUT						; Get input
	mov input, toLCD
	rcall ANSWER						; Check whether input is correct or no
	rjmp GET_ANSWER						; loop again

RESTART:								; Method to RESTART
	rcall TURN_OFF_TIMER				; Turn off timer
	ldi temp, 0				
	mov questionOrder, temp 			; Set question order to 0
	rcall DELAY_02 						; Delay
	ret

CHANGE_WORD:							; Method to Change the Question (Word)
	rcall TURN_OFF_TIMER				; Turn off timer
	ldi temp, 5 						; Add 5 to questionOrder register
	add questionOrder, temp				
	rcall DELAY_02 						; Delay
	rcall DELAY_02
	rcall DELAY_02
	ret

LOSE: 									; Lose Condition
	PRINT_FINAL_MESSAGE losemessage		; Write lose message to LCD

WIN:									; Win Condition
	PRINT_FINAL_MESSAGE winmessage          ; Write win message to LCD

FOREVER:
	rjmp FOREVER


;====================================================================
; PROGRAM MEMORY READ
;====================================================================
WELCOME_AND_CATEGORY_TEXT:
	MOVE_CURSOR 0x8C 				; Move cursor
	ldi ZH,high(2*welcome1) 		; Load high part of byte address into ZH (welcome message)
	ldi ZL,low(2*welcome1) 			; Load low part of byte address into ZL
	rcall LOADBYTE 					; Load Message to LCD
	
	MOVE_CURSOR 0xC8 				; Move cursor
	ldi ZH,high(2*welcome2) 		; Load high part of byte address into ZH
	ldi ZL,low(2*welcome2) 			; Load low part of byte address into ZL
	rcall LOADBYTE 					; Load Message to LCD

	rcall DELAY_02 					; Delay
	rcall DELAY_02
	rcall CLEAR_LCD 				; Clear LCD
	ldi ZH,high(2*user_input) 		; Load high part of byte address into ZH (hangman type message)
	ldi ZL,low(2*user_input) 			; Load low part of byte address into ZL
	rcall LOADBYTE 					; Load Message to LCD
	ret

QUESTION_TYPE:
	rcall CLEAR_LCD
	mov temp, type
	cpi temp, 0X41
	breq QUESTION_TYPE_1 			; If type == "A", write "Hewan" to LCD
	

QUESTION_TYPE_1:
	PRINT_QUESTION_TYPE 0x91, type1 		; write "input" to LCD
	ret


FLASH2RAM:  						; Method to store word from program memory to data memory
	;lpm  							; Load
	;st Y+, R0 						; Store question
	ldi temp, 0 					; Store 0 to saved answer
	st X+, temp
	;adiw ZL, 1
	dec longWord 					; Decrement question length
	brne FLASH2RAM 					; If length != 0, loop again
	ret

BIN2DEC: 							; Convert each digit from number to binary, and saved each digit to registers (ones and tens)
	mov ones, tens
	ldi temp, 0
	mov tens, temp
 	BIN2DEC_LOOP:
		ldi temp, 10
  		cp ones, temp
  		brlt BIN2DEC_ENDLOOP
  		inc tens
  		mov temp, ones
  		subi temp, 10
		mov ones, temp
  		rjmp BIN2DEC_LOOP
 		BIN2DEC_ENDLOOP:
 		ret

UNDERSCORE_TEXT:					; Write underscore (_ _ _ _ _)
	MOVE_CURSOR 0xCF
	ldi ZH,high(2*question) 		; Load high part of byte address into ZH
	ldi ZL,low(2*question) 			; Load low part of byte address into ZL
	rcall LOADBYTE
	ret

ANSWER:
	MOVE_CURSOR 0xCF 				; Move cursor to the beginning of underscore
	
	ldi longWord, 6
	ldi XH, high(words) 			; Load question
	ldi XL, low(words)

	ldi YH, high(answers) 			; Load saved answer
	ldi YL, low(answers)

	add XL, questionOrder 			; Add question address with value of question order register
	add YL, questionOrder

	rcall CHECK_ANSWER 				; Check Answer
	ret

STORE_ANSWER: 						; If input is right
	inc letterGuessed 				; Increment guessed letter
	st Y, toLCD 					; Store answer to saved answer

ANSWER_IS_RIGHT:
	ldi temp, 1
	mov isRight, temp 				; isRight = 1

WRITE_INPUT:
	sbi LcdCtrlPort, RS 			; SETB RS
	out LcdDataPort, toLCD 			; Write input if it's right/already in saved answer
	sbi LcdCtrlPort, EN 			; SETB EN
	cbi LcdCtrlPort, EN				; CLR EN

	ldi toLCD, 0x20					; Write space
	sbi LcdCtrlPort, RS 			; SETB RS
	out LcdDataPort, toLCD
	sbi LcdCtrlPort, EN 			; SETB EN
	cbi LcdCtrlPort, EN				; CLR EN
	adiw YL, 1 						; Increase Y register

CHECK_ANSWER:
	dec longWord 					; Decrement word legth. If word length == 0. jump to check_false
	tst longWord
	breq CHECK_FALSE
	
	ld letterWord, X+
	ld letterAnswer, Y
	mov toLCD, letterWord
	cp letterAnswer, input 			; If saved answer letter == input, write it
	breq ANSWER_IS_RIGHT

	cp letterWord, letterAnswer 	; if saved answer letter == question letter, write it
	breq WRITE_INPUT

	cp letterWord, input			; If the input is right, store it then write the the letter
	breq STORE_ANSWER
	
	ldi toLCD, 0x5F					; Else, write underscore
	rjmp WRITE_INPUT
	rjmp CHECK_ANSWER				; loop again

CHECK_FALSE:
	ldi temp, 1
	cp isRight, temp				; If isRight == 1, exit
	ldi temp, 0						; Reset isRight
	mov isRight, temp
	breq END_LCD
	lsr lives						; Shift to right, decrease the number of LED that are on (wrong answer)
	out LedPort, lives
	ret

;====================================================================
; LCD
;====================================================================
INIT_LCD:
	cbi LcdCtrlPort, RS 		; CLR RS
	ldi temp, 0x38 				; MOV DATA,0x38 --> 8bit, 2line, 5x7
	out LcdDataPort, temp
	sbi LcdCtrlPort, EN 		; SETB EN
	cbi LcdCtrlPort, EN			; CLR EN
	rcall DELAY_01	

	cbi LcdCtrlPort, RS 		; CLR RS
	ldi temp, $0C 				; MOV DATA,0x0C --> disp ON, cursor OFF, blink OFF
	out LcdDataPort, temp
	sbi LcdCtrlPort,EN			; SETB EN
	cbi LcdCtrlPort,EN			; CLR EN
	rcall DELAY_01

	rcall CLEAR_LCD 			; CLEAR LCD
	cbi LcdCtrlPort, RS 		; CLR RS
	ldi temp,$06 				; MOV DATA,0x06 --> increase cursor, display sroll OFF
	out LcdDataPort,temp
	sbi LcdCtrlPort,EN			; SETB EN
	cbi LcdCtrlPort,EN 			; CLR EN
	rcall DELAY_01
	ret

CLEAR_LCD:
	cbi LcdCtrlPort, RS 		; CLR RS
	ldi temp, $01 				; MOV DATA, 0x01
	out LcdDataPort, temp
	sbi LcdCtrlPort, EN			; SETB EN
	cbi LcdCtrlPort, EN			; CLR EN
	rcall DELAY_01
	ret

LOADBYTE:
	lpm 						; Load byte from program memory to r0

	tst r0 						; If already at the end of messsage, branch to END_LCD (return)
	breq END_LCD

	mov toLCD, r0 				; Write character to LCD
	rcall WRITE_TEXT
	adiw ZL,1 					; Increase Z registers
	rjmp LOADBYTE

END_LCD:
	ret

WRITE_TEXT:
	sbi LcdCtrlPort, RS 	; SETB RS
	out LcdDataPort, toLCD
	sbi LcdCtrlPort, EN 	; SETB EN
	cbi LcdCtrlPort, EN		; CLR EN
	rcall DELAY_01
	ret

;====================================================================
; KEYPAD
;====================================================================

GET_INPUT:
	rcall READ_KEYPAD		; Read keypad, keyVal = 1 + baris*4 + kolom
	tst keyVal
	breq GET_INPUT			; if keyVal = 0 (not pressed), loop again
	rcall GET_ASCII			; Get ASCII value for input		
	ret

READ_KEYPAD:
	ldi keyVal, $1    		; Scanning Row1
	ldi temp2, $EF 			; Make Row 1 low
	out KeyPadPort, temp2 	; Send to KeyPort
	rcall read_col 			; Read Columns

	sbrc keyFlag, pressed 	; If key pressed
	rjmp done 				; Exit the routine

	ldi keyVal, $5 			; Scanning Row2
	ldi temp2, $DF 			; Make Row 2 Low
	out KeyPadPort, temp2 	; Send to KeyPort
	rcall read_col 			; Read Columns

	sbrc keyFlag, pressed 	; If key pressed
	rjmp done 				; Exit from routine

	ldi keyVal, $9 			; Scanning Row3
	ldi temp2, $BF 			; Make Row 3 Low
	out KeyPadPort, temp2 	; Send to KeyPort
	rcall read_col 			; Read columns

	sbrc keyFlag, pressed 	; If key pressed
	rjmp done 				; Exit the routine

	ldi keyVal, $D 			; Scanning Row4
	ldi temp2, $7F 			; Make Row 4 Low
	out KeyPadPort, temp2 	; send to KeyPort
	rcall read_col 			; Read columns

	sbrc keyFlag, pressed 	; If key pressed
	rjmp done 				; Exit the routine

done:
	ret

read_col :
	cbr keyFlag, (1 << pressed) 	; Clear status flag

	sbic cols, col1 				; Check column 1
	rjmp nextcol2 					; If not low, go to column 2

hold :
	sbis cols , col1 				; Wait for key release
	rjmp hold

	sbr keyFlag, (1 << pressed) 	; Set status flag
	ret 					
nextcol2:
	sbic cols , col2 				; Check column 2
	rjmp nextcol3 					; If not low, go to column 3

hold2:
	sbis cols , col2 				; Wait for key release
	rjmp hold2
	
	ldi temp2, 1
	add keyVal, temp2
	
	sbr keyFlag, (1 << pressed) 	; Set status flag
	ret
nextcol3:
	sbic cols , col3 				; Check column 3
	rjmp nextcol4					; If not low, go to column 4

hold3:
	sbis cols , col3 				; Wait for key release
	rjmp hold3
	
	ldi temp2, 2
	add keyVal, temp2

	sbr keyFlag, (1 << pressed) 	; Set status flag
	ret
nextcol4:
	sbic cols , col4 				; Check column 4
	rjmp exit						; If not low, exit

hold4:
	sbis cols , col4 				; Wait for key release
	rjmp hold4
	
	ldi temp2, 3
	add keyVal, temp2

	sbr keyFlag, (1 << pressed) 	; Set status flag
	ret

exit:
	clr keyVal 						; keyVal = 0 (reset)
	cbr keyFlag, (1 << pressed) 	; No key is pressed
	ret


GET_ASCII: 							; Method to get ASCII value from input
	cpi keyVal, 1
	breq INPUT_A	; A
	cpi keyVal, 2
	breq INPUT_B	; B
	cpi keyVal, 3
	breq INPUT_D	; D
	cpi keyVal, 4
	breq INPUT_E	; E
	cpi keyVal, 5
	breq INPUT_H	; H
	cpi keyVal, 6
	breq INPUT_I	; I
	cpi keyVal, 7
	breq INPUT_J	; J
	cpi keyVal, 8
	breq INPUT_K	; K
	cpi keyVal, 9
	breq INPUT_L	; L
	cpi keyVal, 10
	breq INPUT_M	; M
	cpi keyVal, 11
	breq INPUT_N	; N
	cpi keyVal, 12
	breq INPUT_O	; O
	cpi keyVal, 13
	breq INPUT_R	; R
	cpi keyVal, 14
	breq INPUT_S	; S
	cpi keyVal, 15
	breq INPUT_T	; T
	cpi keyVal, 16
	breq INPUT_U	; U

INPUT_A:
	ldi toLCD, 0x41		; ASCII A
	rjmp END_OF_KEYPAD_PRESSED

INPUT_B:
	ldi toLCD, 0x42		; ASCII B
	rjmp END_OF_KEYPAD_PRESSED

INPUT_D:
	ldi toLCD, 0x44		; ASCII D
	rjmp END_OF_KEYPAD_PRESSED

INPUT_E:
	ldi toLCD, 0x45		; ASCII E
	rjmp END_OF_KEYPAD_PRESSED

INPUT_H:
	ldi toLCD, 0x48		; ASCII H
	rjmp END_OF_KEYPAD_PRESSED

INPUT_I:
	ldi toLCD, 0x49		; ASCII I
	rjmp END_OF_KEYPAD_PRESSED

INPUT_J:
	ldi toLCD, 0x4A		; ASCII J
	rjmp END_OF_KEYPAD_PRESSED

INPUT_K:
	ldi toLCD, 0x4B		; ASCII K
	rjmp END_OF_KEYPAD_PRESSED

INPUT_L:
	ldi toLCD, 0x4C		; ASCII L
	rjmp END_OF_KEYPAD_PRESSED

INPUT_M:
	ldi toLCD, 0x4D		; ASCII M
	rjmp END_OF_KEYPAD_PRESSED

INPUT_N:
	ldi toLCD, 0x4E		; ASCII N
	rjmp END_OF_KEYPAD_PRESSED

INPUT_O:
	ldi toLCD, 0x4F		; ASCII O
	rjmp END_OF_KEYPAD_PRESSED

INPUT_R:
	ldi toLCD, 0x52		; ASCII R
	rjmp END_OF_KEYPAD_PRESSED

INPUT_S:
	ldi toLCD, 0x53		; ASCII S
	rjmp END_OF_KEYPAD_PRESSED

INPUT_T:
	ldi toLCD, 0x54		; ASCII T
	rjmp END_OF_KEYPAD_PRESSED

INPUT_U:
	ldi toLCD, 0x55		; ASCII U
	rjmp END_OF_KEYPAD_PRESSED

END_OF_KEYPAD_PRESSED: 				; Prevent rapid fire
    ldi temp, 0x0F
    out KeyPadDDR, temp 			; Set row lines as output
    out KeyPadPort, temp 			; Sending 1s to row lines
    nop
	in temp, cols
	ldi temp2, 0xF0
    out KeyPadDDR, temp2 			; Set column lines as output
    out KeyPadPort, temp2 			; Sending 1s to column lines
    nop
	in temp2, cols 					; Read row result
	and temp, temp2

	cp keyFlag, temp				; Loop as long as the keypad hasn't changed condition
	breq END_OF_KEYPAD_PRESSED
	ret

;====================================================================
; DELAY
;====================================================================
DELAY_01:
; Generated by delay loop calculator
; at http://www.bretmulvey.com/avrdelay.html
;
; Delay 25 000 cycles
; 3ms 125us at 8.0 MHz

    ldi  r18, 33
    ldi  r19, 119
	L1: dec  r19
	    brne L1
	    dec  r18
	    brne L1
    ret

DELAY_02:
; Generated by delay loop calculator
; at http://www.bretmulvey.com/avrdelay.html
;
; Delay 100 000 cycles
; 12ms 500us at 8.0 MHz

    ldi  r18, 130
    ldi  r19, 222
	L2: dec  r19
	    brne L2
	    dec  r18
	    brne L2
    nop
    ret

;====================================================================
; DATA
;====================================================================
welcome1:
.db " W E L C O M E", 0

welcome2:
.db " I N   H A N G M A N!!", 0

user_input:
.db "Input kata: ", 0

type1:
.db "Tebak        Waktu: ", 0

question:
.db "_ _ _ _ _", 0

question1:
.db "Game Is Starting !", 0

winmessage:
.db "You Win! :)", 0

losemessage:
.db "You Lose!", 0

timeisup:
.db "Your time is up!", 0

scoretext:
.db "Score:      High Score: ", 0
