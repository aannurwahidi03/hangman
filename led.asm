CP_KEY_VALUE:
	cp value1, key
	breq LED_OUTPUT
	ret

LED_OUTPUT:
	cpi health, 5
	breq LED_FULL_HEALTH
	cpi health, 4
	breq LED_HEALTH_4
	cpi health, 3
	breq LED_HEALTH_3
	cpi health, 2
	breq LED_HEALTH_2
	cpi health, 1
	breq LED_HEALTH_1
	cpi health, 0
	breq LED_HEALTH_0
	

LED_FULL_HEALTH:
	ldi led_data, 0xF8
	out PORTE, led_data
	rcall DELAY_01
	ldi led_data, 0x00
	out PORTE, led_data
	rcall DELAY_01
	rjmp LED_FULL_HEALTH

LED_HEALTH_4:
	ldi led_data, 0xF0
	out PORTE, led_data
	rcall DELAY_01
	ldi led_data, 0x00
	out PORTA, led_data
	rcall DELAY_01
	rjmp LED_HEALTH_4

LED_HEALTH_3:
	ldi led_data, 0xE0
	out PORTE, led_data
	rcall DELAY_01
	ldi led_data, 0x00
	out PORTE, led_data
	rcall DELAY_01
	rjmp LED_HEALTH_3

LED_HEALTH_2:
	ldi led_data, 0xC0
	out PORTE, led_data
	rcall DELAY_01
	ldi led_data, 0x00
	out PORTA, led_data
	rjmp LED_HEALTH_2

LED_HEALTH_1:
	ldi led_data, 0x80
	out PORTE, led_data
	rcall DELAY_01
	ldi led_data, 0x00
	out PORTE, led_data
	rcall DELAY_01
	rjmp LED_HEALTH_1

LED_HEALTH_0:
	ldi led_data, 0x00
	out PORTE, led_data
